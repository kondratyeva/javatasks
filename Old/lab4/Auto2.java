package JavaCourse.lab4;

/**
 * Created by kondratieva on 11-Apr-16.
 */
//2ой - если скорость автомобиля после поворота меньше чем половина от максимальной то его ускорение на этом отрезке увеличивается в 2 раза
public class Auto2 extends Auto {

    public Auto2(double speedMax, int speedUp, double flex) { //конструктор

        super(speedMax, speedUp, flex); // вызвали конструктор родителя
    }
    double initSpeedUp = getSpeedUp(); // нам нужна переменная для хранения исходного ускорения, т.к. она изменяется только для следующего отрезка

    @Override
    public void turn() { // реализация метода уникалена для каждого вида авто, поэтому определяется в дочернем классе
        setSpeedUp(initSpeedUp);
        setEndSpeed(getStartSpeed() * getFlex()); //рассчитали конечную скорость при выходе из поворота
        if (getEndSpeed() < getSpeedMax()/2){ // проверяем условие
            setSpeedUp(getSpeedUp()*2); // если выполняется - удваиваем ускорение
        }
        setStartSpeed(getEndSpeed()); // сохраняем конечную скорость как начальную для следующего отрезка
    }


}
