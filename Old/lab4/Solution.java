package JavaCourse.lab4;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by kondratieva on 11-Apr-16.
 */
/*После реализации создать несколько наборов автомобилей с разными характеристиками и прогнать их на трассе вывести результаты на экран - в виде списка записей следующего типа:
  Автомобиль 1 прошел трассу за 10 минут 5 секунд
  отсортированного от меньшего результата к большему. Алгоритм сортировки реализовать свой - без использования сторонних библиотек.*/
public class Solution {
    public static void main(String[] args) {
        ArrayList<Auto> autos = new ArrayList<>(); // создали список для объектов Авто
        autos.add(new Auto1(300, 5, 0.5)); // заполнили его объектами разных классов
        autos.add(new Auto2(150, 3, 0.7));
        autos.add(new Auto3(250, 4, 0.3));
        autos.add(new Auto1(350, 6, 0.6));
        autos.add(new Auto2(225, 2, 0.75));
        autos.add(new Auto3(325, 4.2, 0.35));
        for (Auto au: autos) { // для каждого вызвали метод гонки
            au.race();
        }
        sort(autos); // отсортировали список по результатам
        print(autos); // вывели на печать
    }

    private static void sort (ArrayList<Auto> autos){
        for (int i = 0; i < autos.size(); i++){
            long time = autos.get(i).getTime();
            int index = i;
            for (int j = i+1; j < autos.size(); j++){
                if (time > autos.get(j).getTime()){
                    time = autos.get(j).getTime();
                    index = j;
                }
            }
            Auto au = autos.get(i);
            autos.set(i, autos.get(index));
            autos.set(index, au);
        }
    }
    public static void print (ArrayList<Auto> autos){// выводим результат на консоль
        Calendar calendar = Calendar.getInstance(); //создали переменную календарь
        for (int i = 0; i < autos.size(); i++) {
            calendar.setTimeInMillis(autos.get(i).getTime() * 1000); // присвоили ей значение time переведенное в милисекунды
            String sdfmin; // здесь будем хранить темплейт для минут
            String sdfsec; // здесь будем хранить темплейт для секунд
            if (calendar.get(Calendar.MINUTE)%10 == 1) { // склонение слова минута
                sdfmin = "m минуту ";
            } else if (calendar.get(Calendar.MINUTE)%10 == 2 || calendar.get(Calendar.MINUTE)%10 == 3 || calendar.get(Calendar.MINUTE)%10 == 4) {
                sdfmin = "m минуты ";
            } else {
                sdfmin = "m минут ";
            }
            if (calendar.get(Calendar.SECOND)%10 == 1) { // склонение слова секунда
                sdfsec = "s секунду";
            } else if (calendar.get(Calendar.SECOND)%10 == 2 || calendar.get(Calendar.SECOND)%10 == 3 || calendar.get(Calendar.SECOND)%10 == 4) {
                sdfsec = "s секунды";
            } else {
                sdfsec = "s секунд";
            }

            SimpleDateFormat sdf = new SimpleDateFormat(sdfmin + sdfsec); // собрали формат, в котором должно отображаться время
            System.out.println("Автомобиль " + (i+1) + " прошел трассу за " + sdf.format(calendar.getTime())); // собрали строку результата
        }
    }
}
