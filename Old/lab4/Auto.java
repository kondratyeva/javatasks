package JavaCourse.lab4;


/**
 * Created by kondratieva on 11-Apr-16.
 */
/*Даны три водителя с одинаковым навыком вождения (Этим параметром можно пренебречь).
Они используют три разных автомобиля. Трасса состоит из 20 прямых отрезков и поворотов между каждым отрезком.
Каждый автомобиль едет один на трассе. У каждого автомобиля есть 3 характеристики - Максимальная скорость, Ускорение и Маневренность.
  Скорость измеряется в км\ч
  Ускорение измеряется  в м\с
  Маневренность - коэффициент потери скорости в момент поворота от 0 до 1 (при маневренности = 0,4 - во время поворота потеряет 60% скорости (Пример 10*0,4 = 4))
  Каждый прямой отрезок - 2 км
  Во время поворота У каждого автомобиля есть своя особенность
  1ый - если скорость автомобиля на момент поворота больше половины от максимальной - то он получает +0,5% от разницы между половиной от максимальной
  и нынешней к маневренности на этот поворот следующим образом (половина максимальной = 75. Нынешняя равна 90. Разница = 15. Маневренность до срабатывания = 0,5, стала = 0,575)
  2ой - если скорость автомобиля после поворота меньше чем половина от максимальной то его ускорение на этом отрезке увеличивается в 2 раза
  3ий - если автомобиль в момент поворота достиг своей максимальной скорости, то его максимальная скорость увеличивается на 10% до конца гонки
  После реализации создать несколько наборов автомобилей с разными характеристиками и прогнать их на трассе вывести результаты на экран - в виде списка записей следующего типа:
  Автомобиль 1 прошел трассу за 10 минут 5 секунд
  отсортированного от меньшего результата к большему. Алгоритм сортировки реализовать свой - без использования сторонних библиотек.
*/

public abstract class Auto implements Movable, Turnable{ // все авто должны уметь звигаться и поворачивать, но объекты класса авто создаваться не должны
    private double speedMax; // характеристика авто - максимальная скорость
    private double speedUp; // характеристика авто - ускорение
    private double flex; // характеристика авто - маневреность


    public final static int interval = 2000; // размер прямого отрезка, не изменяется
    public final static int numOfIntervals = 20; // количество отрезков, не изменяется
    private long time = 0; // переменная для хранения потраченного времени
    private double startSpeed = 0; // переменная для хранения начальной скорости, с которой автомобиль выходит на каждый следующий отрезок
    private double endSpeed = 0; // переменная для хранения скорости, с которой автомобиль заканчивает движение на отрезке

    public Auto(double speedMax, double speedUp, double flex) { // конструктор класса Auto
        this.speedMax = speedMax;
        this.speedUp = speedUp;
        this.flex = flex;
    }
    public void move (){ // все типы авто одинаково движутся на прямых отрезках, поэтому этот метод отределяется в родительском классе
        setEndSpeed(Math.sqrt(interval*2 + getStartSpeed()*getStartSpeed())); // рассчитываем конечную скорость для отрезка
        if (getEndSpeed() <= getSpeedMax()) { // если она меньше или равна максимальной
            setTime((long) (getTime() + (getEndSpeed() - getStartSpeed()) / getSpeedUp())); // расчитываем время по формуле равноускоренного движения
        } else { // если больше
            setEndSpeed(getSpeedMax()); // присвоили конечной скорости значение максимальной, т.к. авто не может двигаться быстрее
            double s = ((getSpeedMax()*getSpeedMax() - getStartSpeed()*getStartSpeed())/2); // определяем на каком расстоянии авто достигнет макс скорости
            setTime((long) (getTime() + (getSpeedMax() - getStartSpeed()) / getSpeedUp() + (interval-s)/getSpeedMax())); // рассчитываем время: часть расстояния по равноускоренной, часть - равномерное движение на максимальной скорости
        }
        setStartSpeed(getEndSpeed()); // изменили начальную скорость для следующего отрезка (поворота)
    }
    public void race (){ // собрали в гонке все отрезки и повороты - общий метод для всех авто
        for (int i = 1; i < numOfIntervals; i++){
            move();
            turn();
        }
        move();
    }

    public void setSpeedMax(double speedMax) { // преобразовали км/ч в м/с
        this.speedMax = speedMax*10/36;
    }

    public void setSpeedUp(double speedUp) {
        this.speedUp = speedUp;
    }

    public void setFlex(double flex) {
        this.flex = flex;
    }

    public double getSpeedMax() {
        return speedMax;
    }

    public double getSpeedUp() {
        return speedUp;
    }

    public double getFlex() {
        return flex;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setStartSpeed(double startSpeed) {
        this.startSpeed = startSpeed;
    }

    public void setEndSpeed(double endSpeed) {
        this.endSpeed = endSpeed;
    }

    public long getTime() {
        return time;
    }

    public double getStartSpeed() {
        return startSpeed;
    }

    public double getEndSpeed() {
        return endSpeed;
    }
}
