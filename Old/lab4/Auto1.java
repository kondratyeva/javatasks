package JavaCourse.lab4;

/**
 * Created by kondratieva on 11-Apr-16.
 */
/*1ый - если скорость автомобиля на момент поворота больше половины от максимальной - то он получает +0,5% от разницы между половиной от максимальной
и нынешней к маневренности на этот поворот следующим образом (половина максимальной = 75. Нынешняя равна 90. Разница = 15. Маневренность до срабатывания = 0,5, стала = 0,575)*/
public class Auto1 extends Auto {

    public Auto1(double speedMax, double speedUp, double flex) { //конструктор
        super(speedMax, speedUp, flex); // возвали конструктор родителя
    }

    @Override
    public void turn() { // реализация метода уникалена для каждого вида авто, поэтому определяется в дочернем классе
        if (getStartSpeed() > getSpeedMax()/2){ // проверяем условие задания
            setEndSpeed(getStartSpeed() * (getFlex() + (getStartSpeed()-getSpeedMax()/2)*0.005)); // если выполняется - рассчитываем конечную скорость с учетом измененного коэффициента маневренности
        }
        else {
            setEndSpeed(getStartSpeed() * getFlex()); // если нет - с заданным
        }
        setStartSpeed(getEndSpeed()); // сохраняем конечную скорость как начальную для следующего отрезка
    }
}
