package JavaCourse.lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 5. Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
public class Task5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите числа, нажимая Enter. Чтобы закончить ввод, введите слово sum:");
        String s = reader.readLine();
        double sum = 0;
        while (!s.equals("sum")){
            try {
                sum += Double.parseDouble(s);
                s = reader.readLine();
            }catch (NumberFormatException e){
                System.out.println("Неверный ввод. Введите число.");
                s = reader.readLine();
            }
        }
        if (sum-(int)sum == 0){
            System.out.println("Сумма введеных чисел равна: " + (int)sum);
        } else {
            System.out.println("Сумма введеных чисел равна: " + sum);}
    }
}
