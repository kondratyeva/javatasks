package JavaCourse.lab3;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 1. Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль.
public class Task1 {
    public static void main(String[] args) {
        int[] arr = new int[4];
        for (int i = 0; i < arr.length; i++){
            arr[i] = ((int)(Math.random()*1000)-500);
        }
        int min = arr[0];
        for (int i =1; i < arr.length; i++){
            if (min>arr[i]){
                min = arr[i];
            }
        }
        System.out.println("В массиве:");
        for (int i:arr) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("Наименьшим является: " + min);
    }
}
