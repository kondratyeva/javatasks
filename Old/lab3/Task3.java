package JavaCourse.lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 3. Вывести все простые числа от 1 до N. N задается пользователем через консоль
public class Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите, пожалуйста, целое число больше 1:");
        int N = 0;
               while (true){
                   try {
                N = Integer.parseInt(reader.readLine());
                if (N < 1){
                    throw new NumberFormatException();
                }
                break;
                   }catch (NumberFormatException e){
                       System.out.println("Неверный ввод, попробуйте еще раз:");
                   }
            }
        System.out.println("Список простых чисел: ");
        for (int i = 1; i < N; i++){
            boolean b = true;
            if (i < 3){
                b = true;
            }else {
                for (int j = 2; j*j <=i; j++){
                    if (i % j == 0){
                        b = false;
                        break;
                    }
                    }
                }
            if (b) {
                System.out.print(i + " ");
            }
            }

        }

}
