package JavaCourse.lab3;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 2. При помощи цикла for вывести на экран нечетные числа от 1 до 99.
public class Task2 {
    public static void main(String[] args) {
        System.out.println("Список нечетных чисел в интервале от 1 до 99:");
        for (int i = 1; i < 100; i+=2){
            System.out.print(i + " ");
        }
    }
}
