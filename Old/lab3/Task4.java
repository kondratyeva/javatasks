package JavaCourse.lab3;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 4. Дан массив из 4 чисел типа int. Вывести их все
public class Task4 {
    public static void main(String[] args) {
        int[] arr = new int[4];
        for (int i = 0; i < arr.length; i++){
            arr[i] = ((int)(Math.random()*1000)-500);
        }
        System.out.println("массив случайных чисел:");
        for (int i:arr) {
            System.out.print(i + " ");
        }
    }
}
