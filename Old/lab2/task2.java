package JavaCourse.lab2;

/**
 * Created by kondratieva on 06-Apr-16.
 */
/*2) Переведите значение каждого примитивного типа в строку и обратно - выведите результаты на  экран*/

public class task2 {
    public static void main(String[] args) {
        byte bytex = 2;
        short shortx = 80;
        int intx = -777;
        long longx = -35999;
        float floatx = 3.6f;
        double doublex = 333.752;
        char charx = 'g';
        boolean booleanx = true;
        System.out.println("Приведение типов:");
        String byteString = Byte.toString(bytex);
        System.out.println("byte bytex = 2 to String byteString: " + byteString);
        String shortString = Short.toString(shortx);
        System.out.println("short shortx = 80 to String shortString: " + shortString);
        String intString = Integer.toString(intx);
        System.out.println("int intx = -777 to String intString: " + intString);
        String longString = Long.toString(longx);
        System.out.println("long longx = -35999 to String longString: " + longString);
        String floatString = Float.toString(floatx);
        System.out.println("float floatx = 3.6f to String floatString: " + floatString);
        String doubleString = Double.toString(doublex);
        System.out.println("double doublex = 333.752 to String doubleString: " + doubleString);
        String charString = Character.toString(charx);
        System.out.println("char charx = 'g' to String charString: " + charString);
        String booleanString = Boolean.toString(booleanx);
        System.out.println("boolean booleanx = true to String booleanString: " + booleanString);
        System.out.println();
        System.out.println("String byteString: 2 to byte: " + Byte.parseByte(byteString));
        System.out.println("String shortString: 80 to short: " + Short.parseShort(shortString));
        System.out.println("String intString: -777 to int: " + Integer.parseInt(intString));
        System.out.println("String longString: -35999 to long: " + Long.parseLong(longString));
        System.out.println("String floatString: 3.6 to float: " + Float.parseFloat(floatString));
        System.out.println("String doubleString: 333.752 to double: " + Double.parseDouble(doubleString));
        System.out.println("String charString: g to char: " + charString.charAt(0));
        System.out.println("String booleanString: true to boolean: " + Boolean.parseBoolean(booleanString));

    }
}
