package JavaCourse.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by kondratieva on 07-Apr-16.
 */
/*4) Написать программу которая бы запросила пользователя ввести число,
     затем знак математической операции, затем еще число и вывела результат.
     Должно работать и для целых и для дробных чисел
     +
     при выводе результата - если число целое, то что бы оно выводилось как целое, а не дробное с 0 после запятой*/

public class task4_1 {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число:");
        String firstString = reader.readLine();
        double firstNum = Double.parseDouble(firstString);
        System.out.println("Введите знак математической операции:");
        String secondString = reader.readLine();
        System.out.println("Введине второе число:");
        String thirdString = reader.readLine();
        double secondNum = Double.parseDouble(thirdString);
        System.out.print("Результат операции равен: ");
        switch (secondString){
            case "+":{
                double result = firstNum + secondNum;
                if (result-(int)result == 0){
                    System.out.println((int)result);
                }
                else {
                    System.out.println(result);
                }
                break;
            }
            case "-":{
                double result = firstNum - secondNum;
                if (result/(int)result == 1){
                    System.out.println((int)result);
                }
                else {
                    System.out.println(result);
                }
                break;
            }
            case "*":{
                double result = firstNum * secondNum;
                if (result-(int)result == 0){
                    System.out.println((int)result);
                }
                else {
                    System.out.println(result);
                }
                break;
            }
            case "/":{
                double result = firstNum / secondNum;
                if (result-(int)result == 0){
                    System.out.println((int)result);
                }
                else {
                    System.out.println(result);
                }
                break;
            }
            case "%":{
                double result = firstNum % secondNum;
                if (result-(int)result == 0){
                    System.out.println((int)result);
                }
                else {
                    System.out.println(result);
                }
                break;
            }
            default:{
                System.out.println("Знак не распознан.");
            }
        }

    }
}
