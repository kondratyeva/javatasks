package JavaCourse.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by kondratieva on 06-Apr-16.
 */
/*4) Написать программу которая бы запросила пользователя ввести число,
     затем знак математической операции, затем еще число и вывела результат.
     Должно работать и для целых и для дробных чисел*/
public class task4 {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число:");
        String firstString = reader.readLine();
        double firstNum = Double.parseDouble(firstString);
        System.out.println("Введите знак математической операции:");
        String secondString = reader.readLine();
        System.out.println("Введине второе число:");
        String thirdString = reader.readLine();
        double secondNum = Double.parseDouble(thirdString);
        System.out.print("Результат операции равен: ");
        switch (secondString){
            case "+":{
                System.out.println(firstNum + secondNum);
                break;
            }
            case "-":{
                System.out.println(firstNum - secondNum);
                break;
            }
            case "*":{
                System.out.println(firstNum * secondNum);
                break;
            }
            case "/":{
                System.out.println(firstNum / secondNum);
                break;
            }
            case "%":{
                System.out.println(firstNum % secondNum);
                break;
            }
            default:{
                System.out.println("Знак не распознан.");
            }
        }

    }
}
