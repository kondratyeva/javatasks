package JavaCourse.lab2;

/**
 * Created by kondratieva on 06-Apr-16.
 */
/*3) Переведите число с точкой в тип без точки и наоборот - выведите результаты*/
public class task3 {
    public static void main(String[] args) {
        float fx = 333.33333f;
        double dx = 8888888.888888;
        int ifx;
        int idx;
        ifx = (int)fx;
        System.out.println("Приведение числовых типов:");
        System.out.println("float fx = 333.33333f to int ifx: " + ifx);
        idx = (int)dx;
        System.out.println("double dx = 8888888.888888 to int idx: " + idx);
        System.out.println("int ifx = 333 to float: " + (float)ifx);
        System.out.println("int idx: 8888888 to double: " + (double)idx);
    }
}
