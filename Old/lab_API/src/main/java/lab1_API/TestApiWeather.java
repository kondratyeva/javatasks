package lab1_API;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kondratieva on 16-Jun-16.
 */

/*      Используя инпут дату (не менее 10 вариаций инпута) из текстового документа в формате
        URL   Expected
        получить респонз и в выбранном вами теге проверить наличие Expected в нем
        Все в формате Junit тестов
        При желании можно применять сериализацию и любые доступные библиотеки по работе с апихами*/

public class TestApiWeather {
    public static List<String> urls = new ArrayList<String>();
    public static List<String> expected = new ArrayList<String>();

    public static void readFile (String fileName) {
        BufferedReader fileReader;
        try {
            fileReader = new BufferedReader(new FileReader(fileName));
            String line = "";
            while (fileReader.ready()) { // читаем построчно файл
                line = fileReader.readLine();
                String[] parts = line.split("   "); // парсим
                urls.add(parts[0]); // линки складываем отдельно
                expected.add(parts[1]); // резалты отдельно
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getResponse(String stringUrl) { // полусаем респонсе по URL
        String response = "";
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                response += line + "\n";
            }
            reader.close();
            conn.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } return response;
    }

    public static Document stringToXml (String string){ // преобразуем файл в xml документ
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        Document xmlResponse = null;
        try {
            docBuilder = builderFactory.newDocumentBuilder();
            // парсим переданную на вход строку с XML разметкой
            xmlResponse = docBuilder.parse(new InputSource(new StringReader(string)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlResponse;
    }
    public static List<String> findData (Document xmlResponse, String xPathSearch){ // собирем с списке все результаты поиска по xpath
        List<String> list = new ArrayList<String>();
        try{
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xPath = xPathFactory.newXPath();
            XPathExpression xPathExpression = xPath.compile(xPathSearch);
            NodeList nodes = (NodeList) xPathExpression.evaluate(xmlResponse, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++){
                list.add(nodes.item(i).getNodeValue());
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return list;
    }

}
