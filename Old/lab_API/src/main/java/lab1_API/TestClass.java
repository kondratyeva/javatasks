package lab1_API;


import org.junit.*;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by kondratieva on 17-Jun-16.
 */
public class TestClass {
    public static String fileName = "source.txt";
    public static String xPathSearch = "//temperature/@unit | //minTemperature/@unit | //maxTemperature/@unit ";
    public static int count;

    @BeforeClass
    public static void beforeClassDo(){
        TestApiWeather.readFile(fileName);
        count = 0;
    }

    @Test // тест по сути один должен быть, но я не знаю, как его прогнать по всем URL без BDD
    public void checkTempUnit1(){
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
            }
    }

    @Test
    public void checkTempUnit2(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit3(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit4(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit5(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit6(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit7(){
        count--;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit8(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit9(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

    @Test
    public void checkTempUnit10(){
        count++;
        String response = TestApiWeather.getResponse(TestApiWeather.urls.get(count));
        Document xmlResponse = TestApiWeather.stringToXml(response);
        List<String> list = TestApiWeather.findData(xmlResponse, xPathSearch);
        for (int j = 0; j < list.size(); j++){
            assertEquals("For URL " + TestApiWeather.urls.get(count)+" temperature unit is not " + TestApiWeather.expected.get(count), TestApiWeather.expected.get(count), list.get(j));
        }
    }

}
