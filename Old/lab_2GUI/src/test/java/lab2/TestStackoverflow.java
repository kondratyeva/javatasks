package lab2;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by kondratieva on 30-May-16.
 */
/* 1) Проверить, что количество в табе ‘featured’ главного блока сайта больше 300
a.	Кликнуть на главной странице Sing Up, проверить что на открывшейся странице присутствуют кнопки для входа из социальных сетей - Google, Facebook
b.	Кликнуть на главной странице любой вопрос из секции Top Questions и на открывшейся странице проверить, что вопрос был задан сегодня.
c.	Дополнительно: проверить, что на главной странице stackoverflow есть предложение о работе с зарплатой больше $ 100k :) (тест должен упасть, если такого предложения нет).
*/
public class TestStackoverflow {
    public WebDriver driver;
    @Before
    public void beforeTestDo(){
        driver = new FirefoxDriver(); //запускаем драйвер
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // просим его каждый раз ждать 5 сек, чтобы прогрузилась страница
        driver.get("http://stackoverflow.com/"); // переходим на страницу сайта
        driver.manage().window().maximize(); // разворачиваем окно
    }

    @Test
    public void testFeaturedNumber(){
        WebElement el_count = driver.findElement(By.xpath(".//*[@class= 'bounty-indicator-tab']")); // получаем элемент хранящий значение featured
        assertTrue("Featured are less than 300", Integer.parseInt(el_count.getText())>300); // достаем из него текст, парсим в число и сравниваем с заданным значением
    }

    @Test
    public void checkSocialLinks(){
        WebElement btn_singUp = driver.findElement(By.xpath(".//*[@id='tell-me-more']")); //находим Sing Up
        btn_singUp.click(); // кликаем
        WebElement btn_google = null; // объявляем элементы
        WebElement btn_facebook = null;
        try{
            btn_google = driver.findElement(By.xpath(".//*[@data-provider='google']/div[@class='text']")); //ищем их
            btn_facebook = driver.findElement(By.xpath(".//*[@data-provider='facebook']/div[@class='text']"));
        }catch (Exception e){}
        assertNotNull("Google button is not found", btn_google); // проверяем, что элементы нашлись и були присвоены переменным
        assertNotNull("Facebook button is not found", btn_facebook);
    }

    @Test
    public void checkQuestionDate(){
        List<WebElement> questions = driver.findElements(By.xpath(".//*[@id='question-mini-list']//h3/a")); // складываем в список все вопросы на странице
        int i = (int) (Math.random()*questions.size()); // ищем случайный индекс списка
        questions.get(i).click(); // кликаем на вопрос под этим номером
        WebElement txt_askedDate = driver.findElement(By.xpath(".//*[@id='qinfo']//p[contains(text(), 'asked')] /../following-sibling::*//b")); // находим элемент который хранит когда был задан вопрос
        assertEquals("Question is asked not today", "today", txt_askedDate.getText()); // достаем из него текст и сравниваем
    }

    @Test
    public void checkWorkOver100(){
        List<WebElement> offers = driver.findElements(By.xpath(".//*[@class='jobs']//div[@class='title']")); // сохраняем в список заголовки всех предложений
        boolean check = false; // вводим булевую переменную, чтобы знать выполнилось условие или нет
        for (int i = 0; i<offers.size(); i++){ // просматривам список
            String s = offers.get(i).getText().trim(); // берем текст из заголовка
            Pattern p = Pattern.compile("\\d+(K)"); // задаем паттерн - нам нужно число, стоящее перед К
            Matcher m = p.matcher(s);
            String result = ""; // объявляем переменную, куда сложим найденный результат
            while (m.find()){
                result = m.group(); // ищем
            }
            // result = s.substring(s.indexOf('$')+1, s.length()) // поскольку структура текста заголовка стандартная, то можно было бы найти нужную часть гораздо проще
            try {
                int sum = Integer.parseInt(result.substring(0, result.length()-1)); // парсим сумму
                if (sum > 100) { // сравниваем с заданным значением
                    check = true; // если условие выполнено,хоть 1 раз
                    break; // дальше искать не нужно, прерываем цикл
                }
            }catch (Exception e){}
        }
        assertTrue("Salary over 100 is not found", check); // в зависимости от значения check делаем проверку
    }

    @After
    public void afterTestDo(){

        driver.close();
    }

}
