package lab2;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by kondratieva on 30-May-16.
 */

/* 1) Проверить наличие лого розетка в левом верхнем углу главной страницы
    a.	Проверить,что меню каталога товаров содержит пункт ‘Apple’
    b.	Проверить, что меню каталога товаров сождержит секцию. Содержащую ‘MP3’
    c.	Кликнуть на секцию “Город” рядом с логтипом сайта, проверить что там присутствуют линки на города “Харьков”, "Одесса”, “Киев”
    d.	Перейти в Корзину и проверить что она пуста */

public class TestRozetka {
    private WebDriver driver;

    @BeforeClass
    public static void beforeClsaaDo(){
        // здесь бы мы делали какие-то настройки, необходимые для прохождения всех тестов, но их у нас пока нет
    }

    @Before
    public void beforeTestDo(){
        driver = new FirefoxDriver(); //запускаем драйвер
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // просим его каждый раз ждать 5 сек, чтобы прогрузилась страница
        driver.get("http://rozetka.com.ua/"); // открываем розетку
        driver.manage().window().maximize(); // разворачиваем окно
    }

    @Test
    public void checkLogo(){ // проверяем наличие логотипа
        WebElement img_header_logo=null; // объект логотипа равен нулю
        try {
            img_header_logo = driver.findElement(By.xpath(".//*[@class = 'logo']/img")); // пытаемся найти объект на странице
        } catch (Exception e){}
        assertNotNull("Logo is not found", img_header_logo); // проверяем, найден ли и присвоен объект переменной
    }

    @Test
    public void checkAppleItem(){ // проверяем наличие Apple в меню
        WebElement lnk_mainManu_Apple=null;// объявляем вебэлемент
        try {
            lnk_mainManu_Apple = driver.findElement(By.xpath(".//*[@id='m-main']//a[contains(text(), 'Apple')]")); // ищем его на странице, по равенству текста не находит((
        }catch (Exception e){}
        assertEquals("Apple item is not found in main menu", "Apple", lnk_mainManu_Apple.getText().trim()); // так что проверяем текст на совпадение здесь
    }

    @Test
    public void checkMP3ContaningItem(){ // проверяем наличие пункта с MP3
        WebElement lnk_mainMenu_MP3=null; // jобъявляем элемент
        try {
            lnk_mainMenu_MP3 = driver.findElement(By.xpath(".//*[@class='m-main']//*[contains(text(),'MP3')]")); // ищем его не странице
        } catch (Exception e){}
        assertNotNull("Item contaning MP3 is not found", lnk_mainMenu_MP3); // проверяем, что нашелся и присвоился
    }

    @Test
    public void checkCityLinks() { // проверяем города
        WebElement lnk_selectCity = driver.findElement(By.xpath(".//*[@id='city-chooser']/a")); // находим элемент города
        lnk_selectCity.click(); // кликаем на него
        List<WebElement> webElements = driver.findElements(By.xpath(".//*[@class='header-city-i']/a | .//*[@class='header-city-i']/span")); // находим все города
        String errorText = "Одесса не найден; Киев не найден; Харьков не найден"; // проверочный стринг
        //можно было бы посчитать, но тогда не понятно было бы чего не хватает, или булеаны на каждый случай заводить, но я так сделала
        for (int i = 0; i<webElements.size(); i++){
            String s = webElements.get(i).getText();
            if (webElements.get(i).getText().equals("Одесса")){
                errorText = errorText.replace("Одесса не найден; ", ""); // если город находится, мы удаляем его из стринга
            }if (webElements.get(i).getText().equals("Харьков")){
                errorText = errorText.replace("Харьков не найден", "");
            }if (webElements.get(i).getText().equals("Киев")){
                errorText = errorText.replace("Киев не найден; ", "");
            }
        }
        assertEquals(errorText, "", errorText); // проверяем если стринг не нулевой, значит какой-то город не нашелся и выводим какой
    }
    @Test
    public void basketIsEmpty(){ // проверям корзину
        WebElement lnk_header_basket = driver.findElement(By.xpath(".//*[@href='https://my.rozetka.com.ua/cart/']")); // находим элемент корзины
        lnk_header_basket.click(); // открываем ее
        WebElement txt_basketStatus = null; // объявляем элемент
        try {
            txt_basketStatus = driver.findElement(By.xpath(".//*[contains(text(), 'Корзина пуста')]")); // и проверяем есть ли на странице элемент, говорящий, что корзина пустая
        } catch (Exception e){}
        assertNotNull("Basket is not empty", txt_basketStatus); // проверяем, что элемент найден и присвоен
    }

    @After
    public void afterTestDo(){
        driver.close(); // закрываем браузер
    }

    @AfterClass
    public static void afterClassDo(){
        // тут бы вернули на место настройки))
    }
}
