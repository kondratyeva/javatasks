package lab3.Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class CityChoice {
    private WebDriver driver;

    public CityChoice(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='header-city-i']/a | .//*[@class='header-city-i']/span") // назодим все города
    List<WebElement> cityLinks;

    public String checkCityDisplayed(List<String> cities){
        Map<String, Boolean> cities1 = new HashMap<String, Boolean>(); // создаем мар город - найден/нет
        String result = "";
        for (String ss: cities) { // заполняем  мар зпаданный город - false
            cities1.put(ss, false);
        }
        for (Map.Entry pair: cities1.entrySet()) { //перебераем мар
            for (int i = 0; i < cityLinks.size(); i++) { // перебираем все найденные города
                if (pair.getKey().equals(cityLinks.get(i).getText())) { // если город из мар есть в списке найденных городом, но значение меняется на true
                    pair.setValue(true);
                }
            }
        }
        for (Map.Entry pair: cities1.entrySet()){ // ищем все города, которые не были найденны и имеют значение false
            if (pair.getValue().equals(false)) {
                result = result + pair.getKey() + " не найден; "; // и добавляем их в стринг
            }
        }
        return result;
    }
}
