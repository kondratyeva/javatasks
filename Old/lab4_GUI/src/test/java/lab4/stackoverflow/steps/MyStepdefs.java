package lab4.stackoverflow.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab3.Stackoverflow.Question;
import lab3.Stackoverflow.SingUp;
import lab4.stackoverflow.runner.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by kondratieva on 06-Jun-16.
 */
public class MyStepdefs {
    public Question question;
    public SingUp singUp;

    @Given("^I on Stackoverflow start page$")
    public void iOnStackoverflowStartPage() throws Throwable {
        if(!TestRun.driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            TestRun.driver.get("http://stackoverflow.com/");
    }

    @Then("^I see that featured quantity more than (\\d+)$")
    public void iSeeThatFeaturedQuantityMoreThan(int arg0) throws Throwable {
        assertTrue("Featured are less than" + arg0, Integer.parseInt(TestRun.mainPage.el_count.getText())>arg0); // достаем из него текст, парсим в число и сравниваем с заданным значением
    }

    @Then("^I see salaries more than (\\d+)$")
    public void iSeeSalsriesMoreThan(int arg0) throws Throwable {
        assertTrue("Salary over " + arg0 + " is not found", TestRun.mainPage.findOffersBySum(arg0)); // в зависимости от значения check делаем проверку
    }

    @When("^I click Sign Up btn$")
    public void iClickSignUpBtn() throws Throwable {
        singUp = TestRun.mainPage.navigateToSingUp(); // создаем объект SingUp, браузер переходит на SingUp
    }

    @When("^I click any question link$")
    public void iClickAnyQuestionLink() throws Throwable {
        question = TestRun.mainPage.navigateToQuestionPage();
    }

    @Then("^I see \"([^\"]*)\" in Asked section$")
    public void iSeeInAskedSection(String arg0) throws Throwable {
        assertEquals("Question is asked not today", arg0, question.txt_askedDate.getText());
    }


    @Then("^I see Google btn$")
    public void iSeeGoogleBtn() throws Throwable {
        assertNotNull("Google button is not found", singUp.btn_google); // проверяем, что элементы нашлись и були присвоены переменным
    }

    @Then("^I see Facebook btn$")
    public void iSeeFacebookBtn() throws Throwable {
        assertNotNull("Facebook button is not found", singUp.btn_facebook);
    }
}
