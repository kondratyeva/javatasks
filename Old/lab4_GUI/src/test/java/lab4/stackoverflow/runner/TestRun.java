package lab4.stackoverflow.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lab3.Stackoverflow.MainPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by kondratieva on 06-Jun-16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/lab4/stackoverflow/features",
        glue = "lab4.stackoverflow.steps",
        tags = "@testStackoverflow"
)
public class TestRun {
    public static WebDriver driver;
    public static MainPage mainPage;

    @BeforeClass
    public static void BeforeClassDo(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if (!driver.getCurrentUrl().equals("http://stackoverflow.com/")){
            driver.get("http://stackoverflow.com/");
        }
        mainPage = new MainPage(driver);
    }

    @AfterClass
    public static void afterClassDo(){

        //driver.quit();
    }
}

