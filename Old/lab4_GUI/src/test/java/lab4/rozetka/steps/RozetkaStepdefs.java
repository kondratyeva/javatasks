package lab4.rozetka.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab3.Rozetka.Basket;
import lab3.Rozetka.CityChoice;
import lab4.rozetka.runner.*;
import lab3.Rozetka.MainPage;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by kondratieva on 06-Jun-16.
 */
public class RozetkaStepdefs {
    public CityChoice cityChoice;
    public Basket basket;

    @Given("^I on Rozetka start page$")
    public void iOnRozetkaStartPage() throws Throwable {
        if(!TestRunRozetka.driver.getCurrentUrl().equals("http://rozetka.com.ua/"))
            TestRunRozetka.driver.get("http://rozetka.com.ua/");
    }

    @Then("^I see Rozetka logo$")
    public void iSeeRozetkaLogo() throws Throwable {
        assertNotNull("Logo is not found", TestRunRozetka.mainPage.img_header_logo); // проверяем, найден ли и присвоен объект переменной
    }

    @Then("^I see Apple item is displayed in main menu$")
    public void iSeeAppleItemIsDisplayedInMainMenu() throws Throwable {
        String s = "";
        if (TestRunRozetka.mainPage.lnk_mainManu_Apple != null){
            s = TestRunRozetka.mainPage.lnk_mainManu_Apple.getText().trim();
        }
        assertEquals("Apple item is not found in main menu", "Apple", s);
    }

    @Then("^I see item contaning MP(\\d+) in main menu$")
    public void iSeeItemContaningMPInMainMenu(int arg0) throws Throwable {
        assertNotNull("Item contaning MP3 is not found", TestRunRozetka.mainPage.lnk_mainMenu_MP3); // проверяем, что нашелся и присвоился
    }

    @When("^I click City link$")
    public void iClickCityLink() throws Throwable {
        cityChoice = TestRunRozetka.mainPage.navigateToCityChoice(); // открываем cityChoice и создаем объект
    }

    @Then("^I see \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" cities$")
    public void iSeeCities(String arg0, String arg1, String arg2) throws Throwable {
        String errorText = cityChoice.checkCityDisplayed(new ArrayList<String>(Arrays.asList(arg0, arg1, arg2))); // ищем совпадения с заданным списком городов, возвращается стринг со списком не найденых
        assertEquals(errorText, "", errorText); // проверяем если стринг не нулевой, значит какой-то город не нашелся и выводим какой
    }

    @When("^I click Basket link$")
    public void iClickBasketLink() throws Throwable {
        basket = TestRunRozetka.mainPage.navigateToBasket(); // открываем корзину и создаем объект страницы
    }

    @Then("^I see empty status$")
    public void iSeeEmptyStatus() throws Throwable {
        assertNotNull("Basket is not empty", basket.txt_basketStatus); // проверяем, что элемент найден и присвоен
    }
}
