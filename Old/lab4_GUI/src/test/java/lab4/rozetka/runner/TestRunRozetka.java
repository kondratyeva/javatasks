package lab4.rozetka.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lab3.Rozetka.MainPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by kondratieva on 06-Jun-16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/lab4/rozetka/features",
        glue = "lab4.rozetka.steps",
        tags = "@rozetkaTest"
)
public class TestRunRozetka {
    public static WebDriver driver;
    public static MainPage mainPage;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if(!driver.getCurrentUrl().equals("http://rozetka.com.ua/"))
            driver.get("http://rozetka.com.ua/");
        mainPage = new MainPage(driver);
    }

    @AfterClass
    public static void tearDown(){
        driver.quit(); //уничтожает экземпляр драйвера close - закрывает
    }
}
