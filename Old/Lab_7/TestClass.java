import JavaCourse.lab7.MainSerialize;
import JavaCourse.lab7.Serial;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by kondratieva on 28-Apr-16.
 */
public class TestClass{
    @Test
    public void testSerial() throws IOException {
        MainSerialize tester = new MainSerialize();
        tester.serialize();
        File file = new File("D:\\NewJava\\JavaProject\\temp.bin");
        boolean result = file.exists();
        assertTrue(result);
    }
    @Test
    public void testDeserial() throws IOException {
        MainSerialize tester = new MainSerialize();
        tester.serialize();
        Serial sOut = tester.deserialize();
        assertTrue(sOut != null);
    }

    @Test
    public void checkGetVar() throws IOException {
        MainSerialize tester = new MainSerialize();
        Serial serial = new Serial();
        Serial sOut = tester.deserialize();
        tester.refl(sOut);
        assertEquals(tester.var, serial.var);

    }
    @Test
    public void checkCahgeVar() throws IOException {
        MainSerialize tester = new MainSerialize();
        Serial serial = new Serial();
        Serial sOut = tester.deserialize();
        tester.refl(sOut);
        serial.changeVar();
        assertEquals(tester.var1, serial.var);
    }
    @Test
    public void checkParentVar() throws IOException {
        MainSerialize tester = new MainSerialize();
        Serial serial = new Serial();
        Serial sOut = tester.deserialize();
        tester.refl(sOut);
        serial.changeVar();
        assertEquals(tester.var2, serial.superVar);
    }




}
