package JavaCourse.lab7;

import java.io.Serializable;

/**
 * Created by kondratieva on 28-Apr-16.
 */
public class Serial extends ParSerial implements Serializable{
    public int var;
    public Serial(){
        super();
        var = 100;
    }
    public void changeVar(){
        var = 300;
    }
}
