package JavaCourse.lab4;

/**
 * Created by kondratieva on 11-Apr-16.
 */
public interface Movable {
    void move(); // все объекты должны уметь двигаться
}
