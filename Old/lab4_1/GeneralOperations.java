package JavaCourse.lab4;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by kondratieva on 13-Apr-16.
 */
public class GeneralOperations {
    public static void print (ArrayList<Auto> autos){// выводим результат на консоль
        Calendar calendar = Calendar.getInstance(); //создали переменную календарь
        for (int i = 0; i < autos.size(); i++) {
            calendar.setTimeInMillis(autos.get(i).getTime() * 1000); // присвоили ей значение time переведенное в милисекунды
            String sdfmin; // здесь будем хранить темплейт для минут
            String sdfsec; // здесь будем хранить темплейт для секунд
            if (calendar.get(Calendar.MINUTE)%10 == 1) { // склонение слова минута
                sdfmin = "m минуту ";
            } else if (calendar.get(Calendar.MINUTE)%10 == 2 || calendar.get(Calendar.MINUTE)%10 == 3 || calendar.get(Calendar.MINUTE)%10 == 4) {
                sdfmin = "m минуты ";
            } else {
                sdfmin = "m минут ";
            }
            if (calendar.get(Calendar.SECOND)%10 == 1) { // склонение слова секунда
                sdfsec = "s секунду";
            } else if (calendar.get(Calendar.SECOND)%10 == 2 || calendar.get(Calendar.SECOND)%10 == 3 || calendar.get(Calendar.SECOND)%10 == 4) {
                sdfsec = "s секунды";
            } else {
                sdfsec = "s секунд";
            }

            SimpleDateFormat sdf = new SimpleDateFormat(sdfmin + sdfsec); // собрали формат, в котором должно отображаться время
            System.out.println("Автомобиль " + (i+1) + " прошел трассу за " + sdf.format(calendar.getTime())); // собрали строку результата
        }
    }
    public static void sort(ArrayList<Auto> list) {
        Collections.sort(list, new Comparator<Auto>(){
            @Override
            public int compare(Auto a1, Auto a2) {
                return a1.getTime() > a2.getTime() ? 1 : a1.getTime() == a2.getTime() ? 0 : -1;
            }
        });
    }
   /* public static void sort (ArrayList<Auto> autos){
        for (int i = 0; i < autos.size(); i++){
            long time = autos.get(i).getTime();
            int index = i;
            for (int j = i+1; j < autos.size(); j++){
                if (time > autos.get(j).getTime()){
                    time = autos.get(j).getTime();
                    index = j;
                }
            }
            Auto au = autos.get(i);
            autos.set(i, autos.get(index));
            autos.set(index, au);
        }
    }*/
}
