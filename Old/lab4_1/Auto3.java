package JavaCourse.lab4;

/**
 * Created by kondratieva on 11-Apr-16.
 */
// 3ий - если автомобиль в момент поворота достиг своей максимальной скорости, то его максимальная скорость увеличивается на 10% до конца гонки
public class Auto3 extends Auto {

    public Auto3(double speedMax, double speedUp, double flex) { // конструктор
        super(speedMax, speedUp, flex); // вызвали конструктор родителя
    }

    @Override
    public void turn() { // реализация метода уникалена для каждого вида авто, поэтому определяется в дочернем классе
        if (getStartSpeed() == getSpeedMax()){ // проверяем условие
            setSpeedMax(getSpeedMax()*1.1); // если выполняется - изменяем максимальную скорость
        }
        setEndSpeed(getStartSpeed()*getFlex()); // рассчитываем скорость на выходе из поворота
        setStartSpeed(getEndSpeed()); // сохраняем конечную скорость как начальную для следующего отрезка
    }
}
