package JavaCourse.lab4;


import java.util.ArrayList;


/**
 * Created by kondratieva on 11-Apr-16.
 */
/*После реализации создать несколько наборов автомобилей с разными характеристиками и прогнать их на трассе вывести результаты на экран - в виде списка записей следующего типа:
  Автомобиль 1 прошел трассу за 10 минут 5 секунд
  отсортированного от меньшего результата к большему. Алгоритм сортировки реализовать свой - без использования сторонних библиотек.*/
public class Solution {
    public static void main(String[] args) {
        ArrayList<Auto> autos = new ArrayList<>(); // создали список для объектов Авто
        autos.add(new Auto1(300, 30, 0.2)); // заполнили его объектами разных классов
        autos.add(new Auto2(150, 3, 0.7));
        autos.add(new Auto3(250, 4, 0.3));
        autos.add(new Auto1(350, 6, 0.6));
        autos.add(new Auto2(225, 2, 0.75));
        autos.add(new Auto3(325, 4.2, 0.35));
        for (Auto au: autos) { // для каждого вызвали метод гонки
            au.race();
        }
        GeneralOperations.sort(autos); // отсортировали список по результатам
        GeneralOperations.print(autos); // вывели на печать

    }



}
