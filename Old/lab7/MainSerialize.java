package JavaCourse.lab7;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by kondratieva on 28-Apr-16.
 */
public class MainSerialize {
    public static int var, var1, var2;
    public static void main(String[] args) throws IOException {
        serialize();
        Serial sOut = deserialize();
        refl(sOut);
    }
    public static void serialize() throws IOException {
        Serial sIn = new Serial(); //создаем объект
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("D:\\NewJava\\JavaProject\\temp.bin"));
        out.writeObject(sIn); // сериализуем его
        out.flush();
        out.close();
    }

    public static Serial deserialize() throws IOException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("D:\\NewJava\\JavaProject\\temp.bin"));
        Serial sOut = null;
        try {
            return sOut = (Serial)in.readObject();// десериализуем
        } catch (ClassNotFoundException e) {
            System.out.println("Класс не найден.");
            return null;
        }
    }
    public static void refl (Serial sOut){
        try {
            Class c = sOut.getClass(); // определяем класс объекта
            Field fieldVar = c.getField("var"); // находим поле
            var = ((Integer)fieldVar.get(sOut)).intValue(); // достаем его значение
            System.out.println("Значение переменной: " + var);
            Method method = c.getMethod("changeVar"); // ноходим метод
            method.invoke(sOut); // запускаем его
            fieldVar = c.getField("var"); // опять обращаемся к полю класса
            var1 = ((Integer)fieldVar.get(sOut)).intValue(); // достаем его значение для нашего объекта
            System.out.println("Значение переменной после работы метода: " + var1);


            Class parc = sOut.getClass().getSuperclass(); // дастаем суперкласс
            Field fieldVar1 = parc.getField("superVar"); // поле суперкласса
            var2 = ((Integer)fieldVar1.get(sOut)).intValue(); // значение поля
            System.out.println("Значание переменной родительского класса: " + var2);
        } catch (NoSuchMethodException e) {
            System.out.println("Метод не найден.");
        } catch (InvocationTargetException e) {
            System.out.println("Метод не отработал.");
        } catch (IllegalAccessException e) {
            System.out.println("Нет доступа к методу.");
        } catch (NoSuchFieldException e) {
            System.out.println("Поле не найдено.");
        }
    }
}
