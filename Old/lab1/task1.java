package JavaCourse.lab2;

/**
 * Created by kondratieva on 06-Apr-16.
 */
/*   1.	Напишите примеры использования данных операторов и выведите результаты на экран
        a. Арифметические
        b. Сравнения
        с. Логические
        d. Приравнивания*/

public class task1 {
    public static void main(String[] args) {
        int x = 9;
        int y = 3;
        int z = 4;
        boolean b = true;
        System.out.println("Задано x = "+ x + ", y = " + y + ", z = " + z);
        System.out.println();
        System.out.println("a. Арифметические операции:");
        System.out.println("Сложение: x + y = " + (x + y));
        System.out.println("Вычитание: x - y = " + (x - y));
        System.out.println("Умножение: x * y = " + (x * y));
        System.out.println("Деление: x / y = " + (x / y));
        System.out.println("Остаток от деления: x % z = " + (x % z));
        System.out.println();
        System.out.println("b. Операции сравнения:");
        System.out.println("Равно: x == y - " + (x == y));
        System.out.println("Не равно: x != y - " + (x != y));
        System.out.println("Больше: x > y - " + (x > y));
        System.out.println("Меньше: x < y - " + (x < y));
        System.out.println("Больше или равно: x >= y - " + (x >= y));
        System.out.println("Меньше или равно: x <= y - " + (x <= y));
        System.out.println();
        System.out.println("c. Логические:");
        System.out.println("Логическое И: x > y && x != z - " + (x > y && x != z));
        System.out.println("Логическое ИЛИ: x < y || x != z - " + (x < y || x != z));
        System.out.println("Логическое НЕ: b = true; !b - " + !b);
        System.out.println();
        x = z;
        System.out.println("d. Приравнивания: x = z; x = " + x);


    }
}
