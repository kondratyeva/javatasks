package JavaCourse.lab6.Task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.util.ArrayList;

/**
 * Created by kondratieva on 27-Apr-16.
 */
public class MainClass {
    private static ArrayList<Path> fileNames = new ArrayList<>(); // объявляем список, куда будем складывыать найденные пасы
    private static String s ="";
    private static String pathto = "D:\\\\NewJava\\\\Git"; // директория, откуда начинаем поиск
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите часть имени файла:");
        s = reader.readLine(); // считываем данные для поиска
        reader.close(); // закрываем стрим
        FindFiles findFiles = new FindFiles(s, fileNames, pathto); // создаем объякт для поиска файлов
        findFiles.find(); // ищем
        if (fileNames.size() == 0){ // если ничего не нашли
            System.out.println("Ни одного файла не найдено.");
        }
        for (Path path: fileNames) { // для каждого найденного файла
            CopyFile copyFile = new CopyFile(pathto, path); // создаем поток с копированием
            copyFile.start(); // запускаем
        }


    }

}
