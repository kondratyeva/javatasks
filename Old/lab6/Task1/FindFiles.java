package JavaCourse.lab6.Task1;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

/**
 * Created by kondratieva on 27-Apr-16.
 */
public class FindFiles {
    private String s;
    public String pathto;
    private ArrayList<Path> fileNames;
    public FindFiles(String s, ArrayList<Path> fileNames, String pathto){
        this.s = s;
        this.fileNames = fileNames;
        this.pathto = pathto;
    }
    public void find () {
        Path pathSource = Paths.get(pathto);
        try {
            Files.walkFileTree(pathSource, new MyFlileVisitor()); // запускаем поиск по директориям
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class MyFlileVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes fileAttributes) {
            if (String.valueOf(path.getFileName()).contains(s)){ // если найден файл и его имя содержи вверенные данные, то
                fileNames.add(path);} // сохраняем его в список
            return FileVisitResult.CONTINUE;
        }

    }
}
