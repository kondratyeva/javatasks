package JavaCourse.lab6.Task1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by kondratieva on 27-Apr-16.
 */
public class CopyFile extends Thread {
    private String pathto;
    private Path path;
    public CopyFile(String pathto, Path path){
        this.pathto = pathto;
        this.path = path;
    }
    int i = 1; // это номер к новой папке
    public void run() {
            String pathD = String.valueOf(path); // для работы с путем переводим его в стринг
            pathD = pathD.substring(pathto.length()-1); // удаляем неизменную честь (откуда начинали поиск)
            int ind = pathD.indexOf('\\'); // находим куда нужно добавить итерал (к концу имени первой папки перед первым слешем)
            pathD = pathto + "\\" + pathD.substring(0, ind) + i + pathD.substring(ind); // собираем путь с добавлением итерала
            i++; // если я правильно поняла, что его каждый раз нужно увеличивать
            Path pathDist = Paths.get(pathD); // сохраняем в пас путь для нового файла
            File newFile = new File(String.valueOf(pathDist.getParent())); // прописываем, где он будет храниться
            newFile.mkdirs(); // создаем все нужные дериктории
            try {
                Files.copy(path, pathDist); // копируем файл
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
