package JavaCourse.lab6;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by kondratieva on 26-Apr-16.
 */
/*2. Написать программу в которой было бы 4 потока и очередь целых чисел.
Задача потоков: 1 - основной, 2 - если число четное - вывести его на экран,
3 - если число нечетное вывести его на экран,
4 - суммировать все числа и после того как 2 или 3 потоки вывели на экран информацию
вывести сумму всех чисел очереди от 1го до того которое рассматривается сейчас*/

public class Task2 {
    static volatile Queue<Integer> queue = new LinkedList<>(); // создаем очередь
    static volatile boolean cancel = true; // флажок для завершения всех потоков
    static volatile boolean isPrinted = false; // флажок, отработал ли 2 или 3 поток

    public static void main(String[] args) throws InterruptedException {
        Object sync = new Object(); // создаем монитор
        EvenThread thread2 = new EvenThread(sync); //создаем нить для четных
        thread2.start(); // запускаем ее
        OddThread thread3 = new OddThread(sync); // создаем нить для нечетных
        thread3.start(); // запускаем ее
        SumThread thread4 = new SumThread(sync); // создаем нить для суммирования
        thread4.start(); // запускаем ее


        while (cancel) {
            synchronized (sync){
                if (queue.size() != 0){ // если список не пуст
                    queue.poll(); // удаляем первый элемент, чтобы стал виден следующий
                }
                queue.add((int)(Math.random() * 100)); // кладем случайное число в очередь
                sync.notifyAll(); // сообщаем, что мейн справился
                Thread.sleep(250); // даем время подумать остальным
            }
        }
    }

    public static class EvenThread extends Thread {
        private Object sync;
        public EvenThread(Object sync){ // конструктор
            this.sync = sync;
        }
        @Override
        public void run() {
            while (cancel) {
                synchronized (sync){
                    try {
                        sync.wait(); // ждем освобождения мейна
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                int var = queue.element();
                if (var % 2 == 0) { // проверяем на четность
                    System.out.println(currentThread().getName() + "четный: " + var); // выводим
                    isPrinted = true; // вывод был
                }
            }
        }
    }

    public static class OddThread extends Thread {
        private Object sync;
        public OddThread(Object sync){
            this.sync = sync;
        }
        @Override
        public void run() {
            while (cancel) {
                synchronized (sync){
                    try {
                        sync.wait(); // ждем освобождения мейна
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                int var = queue.element();
                if (var % 2 != 0) { // проверяем на нечетность
                    System.out.println(currentThread().getName() + "нечетный: " + var); // выводим
                    isPrinted = true; // вывод был
                }
            }
        }
    }

    public static class SumThread extends Thread {
        int sum;
        private Object sync;
        public SumThread(Object sync){
            this.sync = sync;
            sum = 0;
        }
        @Override
        public void run() {
            while (cancel) {
                synchronized (sync){
                    try {
                        sync.wait(); // ждем освобождения мейна
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                sum += queue.element(); //добавляем к сумме новый элемент
                if (isPrinted){
                    System.out.println(currentThread().getName() + "сумма: " + sum); // выводим сумму
                    cancel = false;} // пора завершить все потоки
            }
        }
    }
}


