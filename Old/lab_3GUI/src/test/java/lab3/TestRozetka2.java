package lab3;

import lab3.Rozetka.Basket;
import lab3.Rozetka.CityChoice;
import lab3.Rozetka.MainPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class TestRozetka2 {
    private WebDriver driver;
    private MainPage mainPage;

    @Before
    public void beforeTestDo(){
        driver = new FirefoxDriver(); //запускаем драйвер
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // просим его каждый раз ждать 5 сек, чтобы прогрузилась страница
        driver.get("http://rozetka.com.ua/"); // открываем розетку
        driver.manage().window().maximize(); // разворачиваем окно
        mainPage = new MainPage(driver); // создаем объект главной страницы
    }

    @Test
    public void checkLogo(){ // проверяем наличие логотипа
        assertNotNull("Logo is not found", mainPage.img_header_logo); // проверяем, найден ли и присвоен объект переменной
    }

    @Test
    public void checkAppleItem(){ // проверяем наличие Apple в меню
        String s = "";
        if (mainPage.lnk_mainManu_Apple != null){
            s = mainPage.lnk_mainManu_Apple.getText().trim();
        }
        assertEquals("Apple item is not found in main menu", "Apple", s); // так что проверяем текст на совпадение здесь
    }

    @Test
    public void checkMP3ContaningItem(){ // проверяем наличие пункта с MP3
        assertNotNull("Item contaning MP3 is not found", mainPage.lnk_mainMenu_MP3); // проверяем, что нашелся и присвоился
    }

    @Test
    public void checkCityLinks() { // проверяем города
        CityChoice cityChoice = mainPage.navigateToCityChoice(); // открываем cityChoice и создаем объект
        String errorText = cityChoice.checkCityDisplayed(new ArrayList<String>(Arrays.asList("Одесса", "Харьков", "Киев"))); // ищем совпадения с заданным списком городов, возвращается стринг со списком не найденых
        assertEquals(errorText, "", errorText); // проверяем если стринг не нулевой, значит какой-то город не нашелся и выводим какой
    }

    @Test
    public void basketIsEmpty(){ // проверям корзину
        Basket basket = mainPage.navigateToBasket(); // открываем корзину и создаем объект страницы
        assertNotNull("Basket is not empty", basket.txt_basketStatus); // проверяем, что элемент найден и присвоен
    }

    @After
    public void afterTestDo(){
        driver.close(); // закрываем браузер
    }

}
