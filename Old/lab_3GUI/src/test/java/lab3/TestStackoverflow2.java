package lab3;

import lab3.Stackoverflow.MainPage;
import lab3.Stackoverflow.Question;
import lab3.Stackoverflow.SingUp;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class TestStackoverflow2 {
    public WebDriver driver;
    public MainPage mainPage;

    @Before
    public void beforeTestDo(){
        driver = new FirefoxDriver(); //запускаем драйвер
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // просим его каждый раз ждать 5 сек, чтобы прогрузилась страница
        driver.get("http://stackoverflow.com/"); // переходим на страницу сайта
        driver.manage().window().maximize(); // разворачиваем окно
        mainPage = new MainPage(driver); // создаем объект главной страницы
    }

    @Test
    public void testFeaturedNumber(){
        assertTrue("Featured are less than 300", Integer.parseInt(mainPage.el_count.getText())>300); // достаем из него текст, парсим в число и сравниваем с заданным значением
    }

    @Test
    public void checkSocialLinks(){
        SingUp singUp = mainPage.navigateToSingUp(); // создаем объект SingUp, браузер переходит на SingUp
        assertNotNull("Google button is not found", singUp.btn_google); // проверяем, что элементы нашлись и були присвоены переменным
        assertNotNull("Facebook button is not found", singUp.btn_facebook);
    }

    @Test
    public void checkQuestionDate(){
        Question question = mainPage.navigateToQuestionPage(); //создаем объект Question, браузер переходит на Question
        assertEquals("Question is asked not today", "today", question.txt_askedDate.getText()); // достаем из него текст и сравниваем
    }

    @Test
    public void checkWorkOver100(){
        assertTrue("Salary over 100 is not found", mainPage.findOffersBySum(100)); // в зависимости от значения check делаем проверку
    }

    @After
    public void afterTestDo(){
        driver.close();
    }
}
