package lab3.Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class SingUp {
    private WebDriver driver;

    public SingUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//*[@data-provider='google']/div[@class='text']") // находим кнопку google
    public WebElement btn_google;

    @FindBy (xpath = ".//*[@data-provider='facebook']/div[@class='text']") // находим кунопку facebook
    public WebElement btn_facebook;

}
