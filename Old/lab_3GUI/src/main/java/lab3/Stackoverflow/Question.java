package lab3.Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class Question {
    public WebDriver driver;

    public Question(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//*[@id='qinfo']//p[contains(text(), 'asked')] /../following-sibling::*//b") //находим поле, где отображается каогда был задан вопрос
    public WebElement txt_askedDate;
}
