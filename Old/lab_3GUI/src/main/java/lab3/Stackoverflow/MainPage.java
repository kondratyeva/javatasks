package lab3.Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class MainPage {
    public WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//*[@class= 'bounty-indicator-tab']")  // находим элемент с указание количества фирес
    public WebElement el_count;

    @FindBy (xpath = ".//*[@id='tell-me-more']") // находим кнопку singUp
    public WebElement btn_singUp;

    @FindBy (xpath = ".//*[@id='question-mini-list']//h3/a") // находи все вопросы
    public List<WebElement> questions;

    @FindBy (xpath = ".//*[@class='jobs']//div[@class='title']") // находим все заголовки предложений о работе
    public List<WebElement> offers;

    public int findRandomQuestion(){ // находим случайно выбраный номер вопроса
        int i = (int) (Math.random()*questions.size());
       return i;
    }

    public Question navigateToQuestionPage(){ //переходим на страницу выбранного вопроса
        questions.get(findRandomQuestion()).click();
        return new Question(driver);
    }

    public SingUp navigateToSingUp(){ // переходим на SingUp страницу
        btn_singUp.click();
        return new SingUp(driver);
    }

    public String getSumFromName(String name){ //разыскиваем сумму в заголовке предложения о работе
        Pattern p = Pattern.compile("\\d+(K)"); // задаем паттерн - нам нужно число, стоящее перед К
        Matcher m = p.matcher(name);
        String result = ""; // объявляем переменную, куда сложим найденный результат
        while (m.find()){
            result = m.group(); // ищем
        }
        // result = s.substring(s.indexOf('$')+1, s.length()) // поскольку структура текста заголовка стандартная, то можно было бы найти нужную часть гораздо проще
        if (result.length()>0){ // если сумма нашлась
        return result.substring(0, result.length()-1); // убираем К
        }
        else return "";

    }
    public boolean findOffersBySum (int sum){ // сравниваем найденные суммы оплаты с заданной
        boolean check = false; // вводим булевую переменную, чтобы знать выполнилось условие или нет
        for (int i = 0; i<offers.size(); i++){ // просматривам список
            String result = getSumFromName(offers.get(i).getText().trim()); // получаем стринг суммы
            try {
                int sum1 = Integer.parseInt(result); // парсим сумму
                if (sum1 > sum) { // сравниваем с заданным значением
                    check = true; // если условие выполнено,хоть 1 раз
                    break; // дальше искать не нужно, прерываем цикл
                }
            }catch (Exception e){}
        } return check;
    }
}
