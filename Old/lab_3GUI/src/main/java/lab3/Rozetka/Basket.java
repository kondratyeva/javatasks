package lab3.Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class Basket {
    private WebDriver driver;

    public Basket(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "./*//*[contains(text(), 'Корзина пуста')]") // ищем объект с текстом, что карзина пуста
    public WebElement txt_basketStatus;


}
