package lab3.Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by kondratieva on 02-Jun-16.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "./*//*[@class = 'logo']/img") // находим логотип
    public WebElement img_header_logo;

    @FindBy(xpath = ".//*[@id='m-main']//a[contains(text(), 'Apple')]") // находим пункт с Apple
    public WebElement lnk_mainManu_Apple;

    @FindBy(xpath = "./*//*[@class='m-main']/*//*[contains(text(),'MP3')]") // находим пункт с MP3
    public WebElement lnk_mainMenu_MP3;

    @FindBy(xpath = "./*//*[@id='city-chooser']/a") // находим линку городов
    public WebElement lnk_selectCity;

    @FindBy(xpath = "./*//*[@href='https://my.rozetka.com.ua/cart/']") // находим линку корзины
    public WebElement lnk_header_basket;

    public CityChoice navigateToCityChoice(){ // открываем поп-ап с городами
        lnk_selectCity.click();
        return new CityChoice(driver);
    }

    public Basket navigateToBasket(){ // открываем корзину
        lnk_header_basket.click();
        return new Basket(driver);
    }
}
