package JavaCourse.lab5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by kondratieva on 20-Apr-16.
 */
/*Реализовать свою вариант List
  Используя свой класс - сделать коллекцию дат на 30 елементов
  Проверить день каждой даты.
  Если это будний день - кинуть свое исключение наследованное от Exception
  Если же это выходной - кинуть свое исключение наследованное от RuntimeException
  Вывести результат в виде
  Дата - будний день \ Дата - выходной*/

public class Solution{


    public static void main(String[] args) {
        MyList dates = new MyList(); //создали список
        for (int i = 0; i < 28; i++){ //набросали туда 28 случайных дат в формате д.м.гггг
            dates.add(((int)((Math.random()*28)+1)) + "." + ((int)((Math.random()*12)+1)) + "." + ((int)((Math.random()*100)+1970)));
        }
        dates.add("23.4.2016"); // добавили парочку тех, которые знаем наверняка
        dates.add("14.4.2016");
        SimpleDateFormat sdfIn = new SimpleDateFormat("d.m.yyyy"); //определили формат для парсинга
        SimpleDateFormat sdfOut = new SimpleDateFormat("dd/mm/yyyy"); // определили формат для вывода
        Calendar c = Calendar.getInstance(); // создали переменную календаря
        for (int i = 0; i < dates.size(); i++) { // для каждого элемента списка
            Date date = null;
            if (dates.get(i) instanceof String) { // если объект представляет собой строку
                try {
                    date = sdfIn.parse((String) dates.get(i)); //пытеамся ее пропарсить и присвоить переменной типа Date
                    c.setTime(date);
                    if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){ // определяем, авляется ли день выходным
                        throw new MyExFromRuntime(sdfOut.format(date)); //если используется конструктор
                    } else { // если нет
                        throw new MyExFromException(sdfOut.format(date)); //если используется конструктор
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (MyExFromException e) {
                    //System.out.println(sdfOut.format(date) + " - будний день"); //если не используется конструктор
                } catch (MyExFromRuntime e){
                    //System.out.println(sdfOut.format(date) + " - выходной"); //если не используется конструктор
                }

            }
        }
    }
}
