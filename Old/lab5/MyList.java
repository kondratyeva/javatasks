package JavaCourse.lab5;

import java.util.*;
import java.util.function.UnaryOperator;

/**
 * Created by kondratieva on 20-Apr-16.
 */
public class MyList implements List{
        Object [] arr;
    public MyList(){
        arr = new Object[0];
    }
    public MyList(int i){
        arr = new Object[i];
    }

    @Override
    //1!!!
    public void sort(Comparator c) {
        Arrays.sort(arr, c);
    }

    @Override
    //2!!!
    public int size() {
        return arr.length;
    }

    @Override
    //3!!!
    public boolean isEmpty() {
        return arr.length==0;
    }

    @Override
    //4!!!
    public boolean contains(Object o) {
        return Arrays.asList(arr).contains(o);
    }

    @Override
    //5!!!
    public Object[] toArray() {
        return arr;
    }

    @Override
    //6!!!
    public Object[] toArray(Object[] a) {
        if (arr.length<=a.length){
            System.arraycopy(arr, 0, a, 0, arr.length);
            return a;
        } else return arr;
    }

    @Override
    //7!!!
    public boolean add(Object o) {
        Object[] arr1 = new Object[arr.length+1];
        System.arraycopy(arr, 0, arr1, 0, arr.length);
        arr1[arr.length] = o;
        arr = arr1;
        return true;
    }

    @Override
    //8!!!
    public boolean remove(Object o) {
        int index = -1;
        for (int i = 0; i < arr.length; i++){
            if (arr[i].equals(o)){
                index = i;
                break;
            }
        }
        if (index>=0){
            Object[] arr1 = new Object[arr.length-1];
            System.arraycopy(arr, 0, arr1, 0, index);
            System.arraycopy(arr, index+1, arr1, index, arr.length-1-index);
            arr = arr1;
        }
        return index>=0;
    }

    @Override
    //9!!!
    public boolean containsAll(Collection c) {
        for (Object o: c) {
            int index = -1;
            for (int j = 0; j < arr.length; j++){
                if (arr[j].equals(o)){
                    index = j;
                }
            }
            if (index == -1){
                return false;
            }
        }
        return true;
    }

    @Override
    //10!!!
    public boolean addAll(Collection c) {
        Object[] arr1 = new Object[arr.length+c.size()];
        System.arraycopy(arr, 0, arr1, 0, arr.length);
        int i = arr.length;
        for (Object o: c) {
            arr1[i]=o;
            i++;
        }
        arr = arr1;
        return true;
    }

    @Override
    //11!!!
    public boolean addAll(int index, Collection c) {
        Object[] arr1 = new Object[arr.length+c.size()];
        System.arraycopy(arr, 0, arr1, 0, index);
        int i = index;
        for (Object o: c) {
            arr1[i]=o;
            i++;
        }
        System.arraycopy(arr, index, arr1, i, arr.length-index);
        arr = arr1;
        return true;
    }

    @Override
    //12!!!
    public boolean removeAll(Collection c) {
        boolean b = false;
        for (Object o: c) {
            int index = -1;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].equals(o)) {
                    index = i;
                    b = true;
                    break;
                }
            }
            if (index >= 0) {
                Object[] arr1 = new Object[arr.length - 1];
                System.arraycopy(arr, 0, arr1, 0, index);
                System.arraycopy(arr, index + 1, arr1, index, arr.length - 1 - index);
                arr = arr1;
            }
        }
        return b;
    }

    @Override
    //13!!!
    public boolean retainAll(Collection c) {
        boolean b1 = false;
        for (int i = 0; i < arr.length; i++) {
            boolean b = true;
            Object a = arr[i];
            int index = i;
            for (Object o: c) {
                if (arr[i].equals(o)) {
                    b = false;
                    break;
                }
            }
            if (b) {
                b1 = true;
                Object[] arr1 = new Object[arr.length - 1];
                System.arraycopy(arr, 0, arr1, 0, index);
                System.arraycopy(arr, index + 1, arr1, index, arr.length - 1 - index);
                arr = arr1;
                i--;
            }
        }
        return b1;
    }

    @Override
    //14!!!
    public void clear() {
        arr = new Object[0];
    }

    @Override
    //15!!!
    public Object get(int index) {
        return arr[index];
    }

    @Override
    //16!!!
    public Object set(int index, Object element) {
        arr[index] = element;
        return arr;
    }

    @Override
    //17!!!
    public void add(int index, Object element) {
        Object[] arr1 = new Object[arr.length+1];
        System.arraycopy(arr, 0, arr1, 0, index);
        arr1[index] = element;
        System.arraycopy(arr, index, arr1, index+1, arr.length-index);
        arr = arr1;
    }

    @Override
    //18!!!
    public Object remove(int index) {
        Object[] arr1 = new Object[arr.length-1];
        System.arraycopy(arr, 0, arr1, 0, index);
        System.arraycopy(arr, index+1, arr1, index, arr.length-1-index);
        arr = arr1;
        return arr;
    }

    @Override
    //19!!!
    public int indexOf(Object o) {
        int j = -1;
        for (int i = 0; i < arr.length; i++){
            if (arr[i].equals(o)){
                j = i;
                break;
            }
        } return j;
    }

    @Override
    //20!!!
    public int lastIndexOf(Object o) {
        int j = -1;
        for (int i = 0; i < arr.length; i++){
            if (arr[i].equals(o)){
                j = i;
            }
        } return j;
    }

    @Override
    //21!!!
    public List subList(int fromIndex, int toIndex) {
        Object[] arr1 = new Object[toIndex-fromIndex];
        System.arraycopy(arr, fromIndex, arr1, 0, toIndex-fromIndex);
        return Arrays.asList(arr1);
    }

    @Override
    public ListIterator listIterator() {
        return new ListItr();
    }

    @Override
    public ListIterator listIterator(int index) {
        return new ListItr();
    }

    @Override
    public Iterator iterator() {
        return new Itr();
    }

    public class Itr implements Iterator{

        int index=0;

        @Override
        public boolean hasNext() {
            try {
                Object o = arr[index+1];
                return true;
            } catch (Exception e){
                return false;
            }
        }

        @Override
        public Object next() {
            return arr[index+1];
        }
    }
    public class ListItr extends Itr implements ListIterator{
        int index=0;

        @Override
        public boolean hasPrevious() {
            try {
                Object o = arr[index-1];
                return true;
            } catch (Exception e){
                return false;
            }
        }

        @Override
        public Object previous() {
            return arr[index-1];
        }

        @Override
        public int nextIndex() {
            return index+1;
        }

        @Override
        public int previousIndex() {
            return index-1;
        }

        @Override
        public void remove() {
            Object[] arr1 = new Object[arr.length-1];
            System.arraycopy(arr, 0, arr1, 0, index);
            System.arraycopy(arr, index+1, arr1, index, arr.length-1-index);
            arr = arr1;
        }

        @Override
        public void set(Object o) {
            arr[index] = o;
        }

        @Override
        public void add(Object o) {
            Object[] arr1 = new Object[arr.length+1];
            System.arraycopy(arr, 0, arr1, 0, index);
            arr1[index] = o;
            System.arraycopy(arr, index, arr1, index+1, arr.length-index);
            arr = arr1;
        }
    }
    /*Comparator comparator = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            if (o1 instanceof Integer && o2 instanceof Integer) {
                return (Integer) o1 > (Integer) o2 ? 1 : o1 == o2 ? 0 : -1;
            } else return 0;
        }
    };*/

}
