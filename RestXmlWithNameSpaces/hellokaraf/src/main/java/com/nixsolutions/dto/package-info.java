@XmlSchema(
        namespace="http://localhost:8181/cxf/nix/rest/person",
        elementFormDefault= XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "person", namespaceURI="http://localhost:8181/cxf/nix/rest/person")
        }
)

package com.nixsolutions.dto;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;