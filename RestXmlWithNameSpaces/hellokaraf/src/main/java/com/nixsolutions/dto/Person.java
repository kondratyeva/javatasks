package com.nixsolutions.dto;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Person", namespace = "http://localhost:8181/cxf/nix/rest/person")
public class Person{
    @JsonIgnore
    @XmlTransient
    private final String NAME_SPACE = "http://localhost:8181/cxf/nix/rest/person";

    @XmlElement(namespace = NAME_SPACE, name = "id")
    private Integer personId;
    @XmlElement(namespace = NAME_SPACE, name = "lastName")
    private String lastName;
    @XmlElement(namespace = NAME_SPACE, name = "firstName")
    private String firstName;
    @XmlElement(namespace = NAME_SPACE, name = "birthDate")
    private String birthDate;

    public Person(){}

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }
}
