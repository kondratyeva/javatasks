package com.nixsolutions.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nixsolutions.dto.Person;
import com.nixsolutions.service.PersonService;
import com.nixsolutions.service.PersonServiceImpl;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

public class HelloRestServiceImpl implements HelloRestService {
    private PersonService personService = new PersonServiceImpl();

    public String getRequest() {
        return "Hello Karaf5!!!!";
    }

    public List<Person> getAllPerson() throws JsonProcessingException, JAXBException {
        return personService.getAll();
    }

    public Response getXmlPerson(Integer personId) throws JsonProcessingException, JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Person.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter sw = new StringWriter();
        m.marshal(personService.getById(personId), sw);
        return Response.status(200).entity(sw.toString()).build();
    }

    public Response getJsonPerson(Integer personId) throws JsonProcessingException {
        return Response.status(200).entity(personService.getById(personId)).build();
    }

    public Response createJsonPerson(Person dtoPerson) throws IOException {
        return Response.status(200).entity(personService.createPerson(dtoPerson)).build();
    }

    public Response createXmlPerson(String dtoPerson) throws IOException, JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Person.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        StringReader reader = new StringReader(dtoPerson);
        Person person = (Person) unmarshaller.unmarshal(reader);
        return Response.status(200).entity(personService.createPerson(person)).build();
    }

}
