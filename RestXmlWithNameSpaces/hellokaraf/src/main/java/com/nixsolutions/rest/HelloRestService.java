package com.nixsolutions.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nixsolutions.dto.Person;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

@Path("rest")
public interface HelloRestService {

    @GET
    @Path("hello")
    String getRequest();

    @GET
    @Path("person")
    @Produces(MediaType.APPLICATION_JSON)
    List<Person> getAllPerson() throws JsonProcessingException, JAXBException;

    @GET
    @Path("person/{personId}/")
    @Produces(MediaType.APPLICATION_XML)
    Response getXmlPerson(@PathParam("personId") Integer personId) throws JsonProcessingException, JAXBException;

    @GET
    @Path("personjson/{personId}/")
    @Produces(MediaType.APPLICATION_JSON)
    Response getJsonPerson(@PathParam("personId") Integer personId) throws JsonProcessingException;

    @POST
    @Path("personjson")
    @Consumes(MediaType.APPLICATION_JSON)
    Response createJsonPerson(Person dtoPerson) throws IOException;

    @POST
    @Path("person")
    @Consumes(MediaType.APPLICATION_XML)
    Response createXmlPerson(String dtoPerson) throws IOException, JAXBException;

}
