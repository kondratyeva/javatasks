package com.nixsolutions.service;

import com.nixsolutions.dto.Person;

import java.util.List;

/**
 * Created by kondratieva on 30.03.17.
 */
public interface PersonService {
    Person getById(int personId);

    int createPerson(Person dtoPerson);

    List<Person> getAll();

}
