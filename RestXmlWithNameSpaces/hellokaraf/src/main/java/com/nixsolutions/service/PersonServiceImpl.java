package com.nixsolutions.service;

import com.nixsolutions.dto.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonServiceImpl implements PersonService {
    private List<Person> personList;

    public PersonServiceImpl() {
        personList = new ArrayList<Person>();
        addFirstPerson();
    }

    public Person getById(int personId) {
        if (personId >= 0 && personId < personList.size()){
        return personList.get(personId);
        }
        return null;
    }

    public int createPerson(Person dtoPerson) {
        personList.add(dtoPerson);
        return personList.size()-1;
    }

    private void addFirstPerson(){
        Person person = new Person();
        person.setFirstName("First");
        person.setLastName("Last");
        person.setPersonId(0);
        person.setBirthDate("2001-01-01");
        personList.add(person);
    }

    public List<Person> getAll() {
        return personList;
    }
}
